package com.javacourse.sergey.task0;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;


public class Base {
    private static final long AMOUNT_OF_DELIMITERS = 4;

    private BaseRecord data = new BaseRecord();
    private ArrayList<BaseRecord> base = new ArrayList<>();

    public Base() {}

    public Base(String fName) {
        FileReader file = null;
        try {
            file = new FileReader(fName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Scanner scan = new Scanner(file);

        while (scan.hasNextLine()) {
            addBaseElement(scan.nextLine());
        }
        try {
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<BaseRecord> getBase() {
        return base;
    }

    public void setBase(ArrayList<BaseRecord> base) {
        this.base = (ArrayList<BaseRecord>) base.clone();
    }

    public BaseRecord getData() {
        return data;
    }

    private void addBaseElement(String str) {
        Integer wordNum = 0;

        long occurrencesCount = str.chars().filter(ch -> ch == ';').count();
        if (occurrencesCount == AMOUNT_OF_DELIMITERS) {
            for (String word : str.split(";")) {
                switch (wordNum++) {
                    case 0:
                        data.setDays(getDifference(word));
                        break;
                    case 1:
                        data.setShopName(word);
                        break;
                    case 2:
                        data.setItem(word);
                        break;
                    case 3:
                        data.setCardNum(Long.parseLong(word));
                        break;
                    case 4:
                        data.setPrice(Integer.parseInt(word));
                        break;
                }
            }
            base.add(data.clone());
        }
        else {
            System.out.println("Wrong data.");
        }
    }

    private int getDifference(String date) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
        long diff = 0;
        try {
            diff = System.currentTimeMillis() -  format.parse(date + ":00").getTime();
        } catch (ParseException e) {
            System.out.println("Data parsing error.");
            e.printStackTrace();
        }
        return getDateCountFromDateTime(diff);
    }

    private int getDateCountFromDateTime(long diff){
        return ((int)(diff / (24 * 60 * 60 * 1000)));
    }

    public void printBase() {
        for (BaseRecord db : getBase()) {
           System.out.println(db.getDays()+"; "+db.getShopName()+"; "+db.getItem()+"; "+db.getCardNum()+"; "+db.getPrice());
        }
        System.out.println();
    }

    public void sortBase(Tester.FieldType field){
        switch (field)
        {
            case DATE:
            {
                base.sort((o2, o1) -> o2.days.compareTo(o1.days));
                break;
            }
            case CARD_NUMBER:
            {
                base.sort((o1, o2) -> o1.cardNum.compareTo(o2.cardNum));
                break;
            }
            default:
                System.out.println("Field not found.");
                break;
        }
    }

    public void sumPrice() {
        ArrayList<BaseRecord> tmpBase = new ArrayList<BaseRecord>();

        for (BaseRecord db : getBase()){
            tmpBase.add(db.clone());
        }

        tmpBase.sort((o1, o2) -> o1.cardNum.compareTo(o2.cardNum));
        for (int i = 0; i < tmpBase.size()-1; i++){
            BaseRecord currentCard = tmpBase.get(i);
            BaseRecord nextCard = tmpBase.get(i+1);
            if (currentCard.getCardNum().equals(nextCard.getCardNum())){
                nextCard.setPrice(currentCard.getPrice() + nextCard.getPrice());
            }
            else{
                System.out.println(currentCard.getCardNum()+"; "+currentCard.getPrice());
            }
        }
        BaseRecord preLastCard = tmpBase.get(tmpBase.size()-2);
        BaseRecord lastCard = tmpBase.get(tmpBase.size()-1);
        if(preLastCard.getCardNum() != lastCard.getCardNum()){
            System.out.println(lastCard.getCardNum()+"; "+lastCard.getPrice());
        }
        System.out.println();
    }
}

