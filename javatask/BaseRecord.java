package com.javacourse.sergey.task0;

class BaseRecord implements Cloneable{
    protected Integer days;
    private String shopName;
    private String item;
    protected Long cardNum;
    private int price;

    protected BaseRecord clone() {
        BaseRecord dataClone = null;
        try {
            dataClone =  (BaseRecord) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println("Error cloning data object.");
            e.printStackTrace();
        }
        return dataClone;
    }

    public Integer getDays() {
        return days;
    }

    public String getShopName() {
        return shopName;
    }

    public String getItem() {
        return item;
    }

    public Long getCardNum() {
        return cardNum;
    }

    public Integer getPrice() {
        return price;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public void setCardNum(long cardNum) {
        this.cardNum = cardNum;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}