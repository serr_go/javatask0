package com.javacourse.sergey.task0;

import java.util.Scanner;

public class Tester {

    public void startTesting(){
        Scanner in = new Scanner(System.in);
        String fileName = in.nextLine();

        Base dataList = new Base(fileName);
        SaleBase baseWthSales = null;
        try {
            baseWthSales = new SaleBase(dataList.getBase());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        dataList.printBase();
        dataList.sortBase(FieldType.DATE);
        dataList.printBase();
        dataList.sortBase(FieldType.CARD_NUMBER);
        dataList.printBase();
        dataList.sumPrice();
        dataList.printBase();

        baseWthSales.countSale();
        baseWthSales.printBase();

        dataList.printBase();
    }

    public enum FieldType  {
        DATE, CARD_NUMBER
    };

}