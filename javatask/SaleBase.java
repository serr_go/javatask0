package com.javacourse.sergey.task0;

import java.util.ArrayList;

public class SaleBase extends Base {


    public SaleBase(String fName){
        super(fName);
    }

    public SaleBase(ArrayList<BaseRecord> base) throws CloneNotSupportedException {
        BaseRecord tmpData = getData();
        for (int i = 0; i < base.size(); i++){
            tmpData.setDays(base.get(i).getDays());
            tmpData.setShopName(base.get(i).getShopName());
            tmpData.setItem(base.get(i).getItem());
            tmpData.setCardNum(base.get(i).getCardNum());
            tmpData.setPrice(base.get(i).getPrice());
            getBase().add(tmpData.clone());
        }
    }

    public void countSale(){
        for (BaseRecord db : getBase()){
            if (db.getPrice() > 100)
            {
                db.setPrice((int)(db.getPrice()* 0.95));
            }
        }
    }
}
